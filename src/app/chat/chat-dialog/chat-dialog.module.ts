import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatDialogComponent } from './chat-dialog.component';
import { FormsModule } from '@angular/forms';
import { ReservationService } from 'src/app/reservation.service';
import { HttpClientModule } from '@angular/common/http';
import { ChatDialogService } from '../chat-dialog.service';
import {
  MatSliderModule,
  MatInputModule,
  MatIconModule,
  MatSelectModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatRippleModule,
  MatSnackBarModule,
  MatButtonModule,
  MatTabsModule
} from '@angular/material';

@NgModule({
  declarations: [ChatDialogComponent],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    MatSliderModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRippleModule,
    MatSnackBarModule,
    MatButtonModule,
    MatTabsModule
  ],
  providers: [ReservationService, ChatDialogService],
  exports: [ChatDialogComponent]
})
export class ChatDialogModule {}
