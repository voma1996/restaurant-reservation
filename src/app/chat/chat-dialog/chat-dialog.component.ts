import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ChatDialogService, Message } from '../chat-dialog.service';
import { Observable } from 'rxjs';
import { scan } from 'rxjs/operators';
import { ReservationService } from 'src/app/reservation.service';

@Component({
  selector: 'app-chat-dialog',
  templateUrl: './chat-dialog.component.html',
  styleUrls: ['./chat-dialog.component.css']
})
export class ChatDialogComponent implements OnInit {
  messages: Observable<Message[]>;
  formValue: string;

  @Input() show: boolean;
  @Output() hide: EventEmitter<any> = new EventEmitter();

  constructor(public chat: ChatDialogService, public resSer: ReservationService) {}

  ngOnInit() {
    // appends to array after each new message is added to feedSource
    this.messages = this.chat.conversation.asObservable().pipe(
      scan((acc, val) => {
        return acc.concat(val);
      })
    );
  }

  sendMessage() {
    this.chat.converse(this.formValue);
    this.formValue = '';
  }

  hidden() {
    this.hide.emit();
  }
}
