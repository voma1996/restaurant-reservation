import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';

import { ApiAiClient } from 'api-ai-javascript/es6/ApiAiClient';

import { HttpClient, HttpParams } from '@angular/common/http';

import { Observable, BehaviorSubject } from 'rxjs';
import { ReservationService } from '../reservation.service';

export class Message {
  constructor(public content: string, public sentBy: string) {}
}

@Injectable({
  providedIn: 'root'
})
export class ChatDialogService {
  // public token: any = environment.dialogflow.angularBot;
  public token: any = '9ee44fe8fe1c466683c11cb0ac3e6087';
  public client = new ApiAiClient({ accessToken: this.token });

  public ApiKeyword: string = 'https://restaurant-reservation-backend.herokuapp.com';
  // public ApiKeyword: string = 'http://192.168.2.229:3001';
  public Tables: string = '/tables';
  public Book: string = '/bookings';

  conversation = new BehaviorSubject<Message[]>([]);

  constructor(public resService: ReservationService, private httpClient: HttpClient) {}

  // Sends and receives messages via DialogFlow
  converse(msg: string) {
    const userMessage = new Message(msg, 'user');
    this.update(userMessage);

    return this.client.textRequest(msg).then(res => {
      const speech = res.result.fulfillment.speech;
      const botMessage = new Message(speech, 'bot');
      const elmnt = document.getElementsByClassName('messanger');

      elmnt[0].scrollTop = elmnt[0].scrollHeight - elmnt[0].clientHeight; // Top

      if (res.result.fulfillment.speech === 'this tables') {
        this.showData(res.result);
      } else {
        this.update(botMessage);
      }
    });
  }

  // Adds message to source
  update(msg: Message) {
    this.conversation.next([msg]);
    const elmnt = document.getElementsByClassName('messanger');

    setTimeout(() => {
      elmnt[0].scrollTop = elmnt[0].scrollHeight - (elmnt[0].clientHeight - 100);
    }, 10);
  }

  showData(result: any) {
    this.resService.rqBookTable(result.parameters).subscribe(
      res => {
        console.log('res', res);

        this.update(new Message('Thanks for your booking :)', 'bot'));
      },
      error => {
        console.error(error);

        if (error.status === 409) {
          this.client.textRequest('book the table').then(res => {
            const speech = res.result.fulfillment.speech;
            const botMessage = new Message(`Sorry, this table is booked for chosen date. ${speech}`, 'bot');

            this.update(botMessage);
          });
        } else if (error.status === 400) {
          this.client.textRequest('book the table').then(res => {
            const speech = res.result.fulfillment.speech;
            const botMessage = new Message(`Sorry, your answers is not true, try again. ${speech}`, 'bot');

            this.update(botMessage);
          });
        }
      }
    );
    this.resService.rqGetBookigs().subscribe();
  }
}
