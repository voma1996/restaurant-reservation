import { Component, OnInit, Input } from '@angular/core';

import * as _ from 'lodash';

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.css']
})
export class TablesComponent {
  @Input() sumPlaces: number;
  @Input() status: any;
  @Input() table: any;

  lodash = _;

  constructor() {}
}
