import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { ReservationService } from './reservation.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import * as moment from 'moment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'restaurant';

  public allBookings: any;

  public resTablesData: any;
  public resTablesData2: any = { data: [] };

  public minDate = new Date();

  public valid: boolean = true;

  public show: boolean = false;

  public reservationForm: FormGroup;
  selectedValue: string;

  times: any[] = [
    { time: '8:00' },
    { time: '9:00' },
    { time: '10:00' },
    { time: '11:00' },
    { time: '12:00' },
    { time: '13:00' },
    { time: '14:00' },
    { time: '15:00' },
    { time: '16:00' },
    { time: '17:00' },
    { time: '18:00' },
    { time: '19:00' },
    { time: '20:00' },
    { time: '21:00' },
    { time: '22:00' },
    { time: '23:00' }
  ];

  public resTablesData$ = new Subject();
  public unsubscribe$ = new Subject();

  constructor(private fb: FormBuilder, public resService: ReservationService, private _snackBar: MatSnackBar) {
    this.reservationForm = this.fb.group({
      name: this.fb.control('', [Validators.required]),
      phone: this.fb.control('', [Validators.required]),
      date: this.fb.control(new Date(), [Validators.required]),
      time: this.fb.control('', [Validators.required])
    });
  }
  public changeDate() {
    if (this.reservationForm.value.date !== null) {
      this.RefreshTablesData();
    }
  }

  public RefreshTablesData() {
    const actualDate = moment(this.reservationForm.value.date)
      .startOf('day')
      .add(moment(this.reservationForm.value.time || '08:00', 'HH:mm').diff(moment(new Date()).startOf('day')))
      .toDate();

    this.resService
      .rqGetTables(actualDate)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((el: any) => {
        this.resTablesData = el;
      });
  }

  public RefreshBookings() {
    this.resService
      .rqGetBookigs()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((el: any) => {
        this.allBookings = el;
      });
  }

  public onSubmit() {
    const endOfWorkingDayDate = moment(this.reservationForm.value.date)
      .startOf('day')
      .add(moment(this.reservationForm.value.time, 'HH:mm').diff(moment(new Date()).startOf('day')))
      .toDate();

    const actualtable = this.resTablesData.data.find(el => el.isSelected);

    if (!actualtable || !this.reservationForm.valid) {
      if (!this.reservationForm.valid) {
        this._snackBar.open('The form is not valid', 'OK', {
          duration: 3000
        });
      }

      if (!actualtable) {
        this._snackBar.open('Please select the table', 'OK', {
          duration: 3000
        });
      }

      return;
    }

    const payload = {
      name: this.reservationForm.value.name,
      phoneNo: String(this.reservationForm.value.phone),
      from: endOfWorkingDayDate,
      table: actualtable._id
    };

    this.resService.rqBookTable(payload).subscribe(el => {
      this.RefreshTablesData();
      this.RefreshBookings();
    });
  }

  public pressTab(table: any) {
    this.resTablesData.data.forEach(el => {
      el.isSelected = false;
    });
    table.isSelected = !table.isSelected;
  }

  public test(time: any) {
    const endOfWorkingDayDate = moment(this.reservationForm.value.date)
      .startOf('day')
      .add(moment(time, 'HH:mm').diff(moment(this.reservationForm.value.date).startOf('day')))
      .toDate();
  }

  public closeReservation(book: any) {
    this.resService.rqCloseBooking(book).subscribe(el => {
      this.RefreshTablesData();
      this.RefreshBookings();
    });
  }

  public deleteReservation(book: any) {
    this.resService.rqDeleteBooking(book).subscribe(el => {
      this.RefreshTablesData();
      this.RefreshBookings();
    });
  }

  changeShow() {
    this.show = !this.show;
  }

  ngOnInit() {
    this.RefreshTablesData();
    this.RefreshBookings();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
