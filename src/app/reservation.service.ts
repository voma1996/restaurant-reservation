import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class ReservationService {
  public ApiKeyword: string = 'https://restaurant-reservation-backend.herokuapp.com';
  // public ApiKeyword: string = 'http://192.168.2.229:3001';
  public Tables: string = '/tables';
  public Book: string = '/bookings';

  public rqGetTables(date = new Date()) {
    return this.httpClient.get(this.ApiKeyword + this.Tables, {
      params: new HttpParams().set('date', date.toISOString())
    });
  }

  public rqBookTable(payload) {
    console.log('was here', payload);
    return this.httpClient.post(this.ApiKeyword + this.Book, payload);
  }

  public rqGetBookigs() {
    return this.httpClient.get(this.ApiKeyword + this.Book);
  }

  public rqCloseBooking(booking, date = new Date()) {
    return this.httpClient.post(`${this.ApiKeyword}${this.Book}/${booking._id}/close`, { to: date });
  }

  public rqDeleteBooking(booking) {
    return this.httpClient.delete(`${this.ApiKeyword}${this.Book}/${booking._id}`);
  }

  constructor(private httpClient: HttpClient) {}
}
